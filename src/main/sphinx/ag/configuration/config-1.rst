The |project| can be configured with the following steps:

* **Step 1.** Provide Business Domain Model Service permissions to users
* **Step 2.** Make the Service available to a proxy service




**Step 1.**

Users need to have permissions to access the Business Domain Model and need permissions on data views.
Give users the rights to use Business Domain Model and read data views through |project| UI.

These permissions are described in the Business Domain Model Service Administration Guide.


**Step 2.**

Make the Service available to a proxy service by specifying:

.. code-block:: console

   proxy all /browse/__graphql to acx-graphql:4000/
   proxy all /browse/ to /acx-web:80/browse

As an example, when a NGINX server is used as a proxy server, the configuration file needs to be updated.
The configuration file of a NGINX server can be found in the folder ``/etc/nginx/available-sites``.
The following lines need to be added to this file:

.. code-block:: console

   location /browse/__graphql {
     # default docker DNS resolver set as resolver here so it can resolve location if you recreate docker container
     resolver 127.0.0.11 valid=30s;
     proxy_pass http://acx-graphql:4000/;
     access_log  /var/log/nginx/access.log  upstream_logging;

     # kill browser cache
     add_header Last-Modified $date_gmt;
     add_header Cache-Control 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';
     if_modified_since off;
     expires off;
     etag off;

     # kill nginx cache
     proxy_no_cache 1;
     # even if cached, don't try to use it
     proxy_cache_bypass 1;

   }


   location /browse/ {
	 # default docker DNS resolver set as resolver here so it can resolve location if you recreate docker container
     resolver 127.0.0.11 valid=30s;
     rewrite ^/browse(/.*)$ /$1 break;
     proxy_pass http://acx-web:80/browse;
     access_log  /var/log/nginx/access.log  upstream_logging;
	 # kill browser cache
     add_header Last-Modified $date_gmt;
     add_header Cache-Control 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';
     if_modified_since off;
     expires off;
     etag off;

	 # kill nginx cache
	 proxy_no_cache 1;
	 # even if cached, don't try to use it
	 proxy_cache_bypass 1;
   }