##################
What is |project|?
##################

|project| belongs to the new collection of Asset Control Services such as Data Cleansing, Data Browsing and the Core UI Service.
They are developed based on the MicroService architecture.

|project| is a Service to maintain a business domain model and to browse data using this model.

This business datamodel will be provided to the |project| from the AC Business Domain Model Service.
This datamodel is using the underlying AC logical data models as the starting point for categorizing the data in terms of Views of Data, Class and Attribute.
It can be seen as an abstraction layer providing a business datamodel.
It gives the ability to alias AC logical data model elements as Views. The following types of Views exist:

* Data Views
* Class Views
* Attribute Views

Views hide the complexity of the logical data model structures stored in the database
to ease the user interaction with the actual data through meaningful descriptions of what the data represents.
BDMS views are configurable to fit the requirements from the business in terms of descriptive view names.

.. figure:: /img/bdms_simple_viewmodel.png
   :alt: BDMS Views: Data, Class, Attribute
   :align: center

   BDMS Views: Data, Class, Attribute


Based on this model users can browse and manipulate data in AC Plus and or ACX via |project|.

When the user selects Data Browsing from the navigation menu, a screen like below is shown.

.. figure:: /img/ac-databrowsing-main.png
   :alt: AC Databrowsing Service UI Main page
   :align: center

   AC Databrowsing Service UI Main page

The user chooses what data to browse by selecting a Data View and Class View, filtering the appropriate data.
The leftside pane of the page shows the data views (Consolidated View, Bloomberg View, etc.).

Class Views vary across each Data View.

For example:

When you select the ``Consolidated View`` you can see the Class Views: Issuer, Instrument Stock, Instrument CDS, etc.

.. figure:: /img/ac-databrowsing-main2.png
   :alt: AC Databrowsing Service UI: Data View and Class View
   :align: center

   AC Databrowsing Service UI: Data View and Class View

Once you select a ``Class View`` like for instance ``Instrument CDS`` the browse result will display the data on the rightside pane of the page.

.. figure:: /img/ac-databrowsing-main3.png
   :alt: AC Databrowsing Service UI: Browse Result
   :align: center

   AC Databrowsing Service UI: Browse Result


#################
How does it work?
#################

The Business Data model is built in AC Plus and can be replicated in ACX.
In AC Plus two extra attributes are added, $DATAMODEL and $CLASS.
Information describing which classes and datamodels do exist is added to the datamodels of the AC Plus interfaces.
The Business Domain Model Service uses this information to import and distribute the business datamodel.
The UI shows the Data Views and Class Views it imported from the Business Domain Model Service.
The user must select a Data View and a Class View in order to discover data

To this model a summary flag is added to all attributes to influence the behavior of |project|.
Only attributes having set this summary attribute flag will be displayed.

The |project| will only display data for which an user is authenticated via AC Authentication Service.
Which data is shown is depending on the user permissions. There can exist for example two users with the following permissions:

 *  User 1 is allowed to see all data defined data models;
 *  User 2 is allowed to see only data from the Consolidated data model and only Equities and only a limited set of attributes for Equity.

############################
AC MicroService Architecture
############################

A MicroServices architecture is a collection of independently deploy-able services.
Every service has its own function and will be deployed in its own Docker container.
Every container has its own isolated workload environment making it independently deploy-able and scalable.
Each individual AC Service runs in its own isolated environment, separate from the others within the architecture.
Some of the new AC Services are Web applications having their own Web User Interfaces (UI). They are developed to
provide the the ability to configure a single unified Web UI (AC Web UI).
The AC Core UI Service is the core component of the AC Services representing the common properties of this UI.
