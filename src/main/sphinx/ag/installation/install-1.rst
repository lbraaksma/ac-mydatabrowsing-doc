*****************************
Minimal Software Requirements
*****************************

* Docker
* Proxy service (e.g. a separate NGNIX container)
* AC Core UI Service
* AC Authentication Service
* Ac Business Domain Service
* AC Server 6 or 7 that includes $CLASS and $DATAMODEL attributes
* AC interface(s) installed that include logic to derive $CLASS and $DATAMODEL

The latter three requirements are needed to make the Business Domain Model available for |project|.
All software requirements must be met prior to installing the |project| . If not, please resolve this first.

******************************
Installing the |project|
******************************

The |project| is provided as one package with two separate Docker image files which you can download from the Asset Control Userweb as follows:

   - Log onto the Asset Control `Userweb <https://services.asset-control.com/userweb>`_. Login
     credentials are required to access this site.
   - In the “AC Plus Applications” section, follow the link for “AC DATABROWSING".
   - Click the link in the "Application builds" section to download the package file.

The docker image files for |project| are contained in a `*.tar.gz` archive.
There is no need to unzip the package just load the Docker image files to your local Docker repository.
For example let's say one of the image names is databrowsing-service:1.0.0 you can use the command:


.. code-block:: console

   docker load -i <databrowsing-service-1.0.0-imagefile>.tar.gz

Like this example load both image files to your repository.

In this Administration Guide we will use the docker-compose utility to get |project| ready for starting.

A docker-compose file,  :file:`docker-compose.yml` would look like:

.. code-block:: console

   web:
     image: "localdockerrepo/acx-web:0.1.0.275"
     hostname: acx-web
     container_name: acx-web
     environment:
       GRAPHQL_URL: http://graphql:4000/graphql/
       REACT_APP_GRAPHQL_SERVER: //_graphql
     ports:
       - "4000:80"
     expose:
       - 80
     networks:
       - acx-net
       - ac-net

   graphql:
     image: "localdockerrepo/acx-graphql:1.0.0.180"
     hostname: acx-graphql
     container_name: acx-graphql
     environment:
       PORT: 4000
       NODE_ENV: production
       AC_BDS_BASE_URL: http://bdms-service:9000/v1/
       LINEAGE_API: http://ac-dl-read:9000
       ACX_BDS_BASE_URL: https://bdms.uat02.acx-sandbox.net/v1/
     networks:
       - acx-net
       - lineage_default
       - ac-net
     command: node --max-http-header-size=80000 dist/index.js
     networks:
       lineage_default:
         external: true
       acx-net:
         external: true
       ac-net:
         external: true

The Web container needs two environment variable to communicate with the other GraphQL container.
The GraphQL container uses two environment variables to communicate with the Business Domain Service of AC Plus and or ACX.
And it needs one optional environment variable to the AC Data Lineage Service if this Service has been installed.

In this example containers are configured to use a virtual docker `ac-net` network and if ACX is used also the `acx-net` network.
Port 80 will be exposed to access the |project| outside the `ac-net` network.
Port 4000 will be used for the GraphQL container to communicate with the Web container.

If AC Data Lineage Service has been installed the GraphQL container also uses its virtual network `lineage-default`.

********************************
Upgrading the |project|
********************************

Stop and remove the container (when using docker-compose) but do not remove the volume.
Run the following command from the folder where your docker-compose file resides:

.. code-block:: console

   docker-compose down


Download the new images from `Userweb <https://services.asset-control.com/userweb>`.
Load the new images to your local Docker repository.
Update the docker-compose file with the names of the new images.


********************************
Uninstall the |project| Service
********************************

To stop and remove the container and remove volume (when using docker-compose).
Run the following command from the folder where your docker-compose file resides:


.. code-block:: console

   docker-compose down --volumes


It is not common practice to remove the image as the old image can be kept without problem.
Look for the `image id`  with command:

.. code-block:: console

   docker images

However if you wish to remove the image the command is:


.. code-block:: console

   rmi imageid

If you have made changes to the configuration file of a proxy server for the |project| do not forget to delete them as well.
