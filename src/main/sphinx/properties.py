# -*- coding: utf-8 -*-
import acdocutils

project = 'Data Browsing Service'
version = '1.0'
release   = acdocutils.get_release_version(version)

# Chapter number prefix text. Default is ''. Uncomment this to override.
chapter_text = 'Section'

latex_title = u'%s \\textsuperscript{\\texttrademark{}} %s' % (project, version)

pn = "ac-data-browsing-doc"

# document subtitle, use 'author' tag to pass in
# Include latex size/formatting commands as needed
ug = 'User Guide'
ag = 'Administration Guide'
rn = 'Release Notes'

latex_documents = [
  ('ug/index', '%s-UG.tex' % pn, latex_title, ug, 'ac-guide'),
  ('ag/index', '%s-AG.tex' % pn, latex_title, ag, 'ac-guide'),
  ('rn/index', '%s-RN.tex' % pn, latex_title, rn, 'ac-guide')
]

extensions = [
#    'ac_ref',
    'ac_appendix',
    'ac_table',
    'acprop',
    'acattr',
    'acerr',
    'acfn',
    'acobj',
    'acop',
    'acprg',
    'actyp',
    'acapiattr',
    'acapiobj']

project_replacements = {
    # Name as known on Userweb
    'product': 'Data Browsing Service',
    # (downloaded package name, with extension variations)
    'package': 'ac-databrowsing-%s-pkg' % release,
    'downloaded_package': 'ac-databrowsing-%s-pkg.tar.gz' % release,
    # Userweb section: 'AC Plus Applications', 'AC Plus Interfaces' or 'AC Plus Normalized and Consolidated Data Models'
    'section': 'AC Data Browsing Service',
}

publish_to = ['webdoc','Applications/%s %s' % (project, version)]
